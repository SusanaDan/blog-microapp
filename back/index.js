
const express = require('express')
const morgan = require('morgan')

const app = express()

app.use(express.static('public'))
app.use(morgan('tiny'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(require('./routes/index.routes'))

app.get('/', (req, res) => {
    res.json({ message: 'Backend' })
})
app.listen('1337')