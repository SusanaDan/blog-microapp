###Susana's Blog - microservices
_Note: the blog entries show up twice_  

*You have to create the network*  
```docker network create mynetwork```
1. The api was done with the help of this tutorial:
https://medium.com/@etiennerouzeaud/how-create-an-api-restfull-in-express-node-js-without-database-b030c687e2ea

2. The reverse proxy was done with the help of this repository:
https://github.com/Rob--W/cors-anywhere
The app just shows some blog entries that are stored in a json format.