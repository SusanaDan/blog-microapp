// Importamos paquetes
const express = require('express')
// App
const app = express()
// Morgan
app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// Iniciamos el servidor
app.listen('1338')
